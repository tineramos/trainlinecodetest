//
//  CSVReaderWriterErrors.h
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 12/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#ifndef CSVReaderWriterErrors_h
#define CSVReaderWriterErrors_h

static NSString * const CSVReaderWriterErrorDomain    =   @"com.Trainline.CSVReaderWriter.ErrorDomain";
static NSString * const CSVReaderErrorDomain          =   @"com.Trainline.CSVReader.ErrorDomain";
static NSString * const CSVWriterErrorDomain          =   @"com.Trainline.CSVWriter.ErrorDomain";

typedef NS_ENUM(NSInteger, CSVReaderWriterError) {
    CSVReaderWriterUnknownFileMode,
    CSVReaderWriterErrorOpenFile
};

typedef NS_ENUM(NSInteger, CSVReaderError) {
    CSVReaderInvalidPath,
    CSVReaderNullFile,
    CSVReaderInvalidFormat,
    CSVReaderInvalidPermission,
};

typedef NS_ENUM(NSInteger, CSVWriterError) {
    CSVWriterInvalidPermission,
};

#endif /* CSVReaderWriterErrors_h */
