//
//  main.m
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 08/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CSVReaderWriter.h"
#import "CSVReader.h"
#import "CSVWriter.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSString *developer = [NSSearchPathForDirectoriesInDomains(NSDeveloperDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *inputFilePath = [developer stringByAppendingString:@"/TrainlineCodeTest/TrainlineCodeTest/code.txt"];
        NSString *outputFilePath = [developer stringByAppendingString:@"/TrainlineCodeTest/TrainlineCodeTest/codetwo.txt"];
        
        CSVReader *reader = [[CSVReader alloc] initWithFilePath:inputFilePath];
        CSVWriter *writer = [[CSVWriter alloc] initWithFilePath:outputFilePath];
        
        CSVReaderWriter *scanner = [[CSVReaderWriter alloc] init];
        
        NSError *scannerError;
        [scanner open:outputFilePath mode:FileModeWrite error:&scannerError];
        [scanner open:inputFilePath mode:FileModeRead error:&scannerError];
        
        [reader openFile];
        
        
//        [reader openFileWithSuccessBlock:^{

        
            // using CSVReaderWriter for writing and CSVReader
            NSArray *columns = [reader readAllColumns];
            NSError *error;
            while (columns) {
                [scanner write:columns error:&error];
                columns = [reader readAllColumns];
            }
            
            [scanner closeOutputStream];
        
             
            /*
            // using CSVReader and CSVWriter
            if ([writer openFile:&error]) {
                NSArray *columns = [reader readColumnsAtRange:NSMakeRange(5, 3)];
                while (columns) {
                    [writer write:columns];
                    columns = [reader readColumnsAtRange:NSMakeRange(5, 3)];
                }
                
                [writer closeFile];
            }
            else {
                NSLog(@"Error opening output file.");
            }
             */
            
            [reader closeFile];
//            
//        } failureBlock:^(NSString *error) {
//            
//        }];
        
//        NSMutableArray *array = [NSMutableArray array];
//        if ([scanner read:array]) {
//            [writer openFileWithSuccessBlock:^{
//                [writer write:array];
//                
//                [scanner closeOutputStream];
//                [writer closeFile];
//            } failureBlock:^(NSString *error) {
//                NSLog(@"Error: %@", error);
//            }];
//            
//        }
        
    }
    return 0;
}
