//
//  Blocks.h
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 08/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#ifndef Blocks_h
#define Blocks_h

typedef void (^VoidBlock)(void);
typedef void (^BoolBlock)(BOOL result);
typedef void (^FailureBlock)(NSString *error);
typedef void (^StringBlock)(NSString *string);
typedef void (^ErrorBlock)(NSError *error);

#endif /* Blocks_h */
