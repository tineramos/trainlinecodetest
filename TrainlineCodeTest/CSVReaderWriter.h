//  CSVReaderWriter.h
//  TrainlineCodeTest

#import <Foundation/Foundation.h>

#import "CSVFileDelimiter.h"

// NS_OPTIONS should only be used for defining bitmask
typedef NS_ENUM(NSInteger, FileMode) {
    FileModeRead = 1,
    FileModeWrite
};

@interface CSVReaderWriter : NSObject

NS_ASSUME_NONNULL_BEGIN

- (void)open:(NSString *)path
        mode:(FileMode)mode DEPRECATED_MSG_ATTRIBUTE("use CSVReader or CSVWriter");

- (BOOL)read:(NSMutableString *_Nonnull *_Nonnull)column1
     column2:(NSMutableString *_Nonnull *_Nonnull)column2 DEPRECATED_MSG_ATTRIBUTE("use CSVReader readColumnsAtIndexes: or readColumnsAtRange:");

- (BOOL)read:(NSMutableArray *)columns DEPRECATED_MSG_ATTRIBUTE("use readColumns:error:");

- (void)write:(NSArray *)columns DEPRECATED_MSG_ATTRIBUTE("use write:error:");

- (void)close DEPRECATED_MSG_ATTRIBUTE("use closeInputStream: or closeOutputStream:");

// MARK: - New methods

- (BOOL)open:(NSString *)path mode:(FileMode)mode error:(NSError **)error;

- (BOOL)readColumns:(NSMutableArray *)columns error:(NSError **)error;

- (BOOL)write:(NSArray *)columns error:(NSError **)error;

- (void)closeInputStream;
- (void)closeOutputStream;

NS_ASSUME_NONNULL_END

@end
