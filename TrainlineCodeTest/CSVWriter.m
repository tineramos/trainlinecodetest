//
//  CSVWriter.m
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 13/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import "CSVWriter.h"

#import "CSVReaderWriterErrors.h"

static char * const CSVReaderNewLine = "\n";

@interface CSVWriter ()
{
    NSOutputStream *outputStream;
    NSString *delimiterString;
}

@property (nonatomic, assign) FileDelimiter delimiter;

@end

@implementation CSVWriter

// MARK: - Designated Initializer

- (instancetype)initWithFilePath:(NSString *)path
{
    if (!path || path.length == 0) {
        return nil;
    }
    
    self = [super init];
    if (self) {
        [self setDelimiter:FileDelimiterTab];
        outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    }
    
    return self;
}

// MARK: - Public methods

- (void)openFile
{
    [self openFileWithSuccessBlock:nil failureBlock:nil];
}

- (void)openFileWithSuccessBlock:(VoidBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
    if (!outputStream) {
        if (failureBlock) {
            failureBlock(@"Can not open file.");
        }
        return;
    }
    
    // to start the streaming of data from the input source.
    [outputStream open];
    
    if (successBlock) {
        successBlock();
    }
}

- (void)write:(NSArray *)columns
{
    NSMutableString *output = [NSMutableString string];
    
    for (int i = 0; i < [columns count]; i++) {
        [output appendString: columns[i]];
        if (i < [columns count] - 1) {
            [output appendString:delimiterString];
        }
    }
    
    [self writeLine:output];
}

- (void)closeFile
{
    if (outputStream) {
        [outputStream close];
        outputStream = nil;
    }
}

#pragma mark - Private methods

- (void)setDelimiter:(FileDelimiter)delimiter
{
    _delimiter = delimiter;
    delimiterString = delimiterArray[_delimiter];
}

- (void)writeLine:(NSString *)line
{
    NSData *data = [line dataUsingEncoding:NSUTF8StringEncoding];
    
    // NOTE: -outputStrem:write:maxLength: - Returns the number of bytes actually written.
    [outputStream write:data.bytes maxLength:data.length];
    
    unsigned char* lf = (unsigned char*)CSVReaderNewLine;
    [outputStream write:lf maxLength: 1];
    
    /*
     Is there a difference between
     
     unsigned char* lf = (unsigned char*)"\n";
     [outputStream write:lf maxLength: 1];
     
     and appending @"\n" after for-loop in -write: method??
     */
}

@end
