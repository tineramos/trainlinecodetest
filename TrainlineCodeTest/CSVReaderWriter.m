//  CSVReaderWriter.m
//  TrainlineCodeTest
//
// REFERENCES:
// Apple Stream Programming Guide:  http://apple.co/2hgqLAC
// For Reference of InputStream:    http://apple.co/2gSOPqe
// For Reference of OutputStream:   http://apple.co/2hntd5V
//
//

#import <Foundation/Foundation.h>

#import "CSVReaderWriter.h"

#import "CSVReaderWriterErrors.h"

// MARK: - Private methods, properties and ivars
@interface CSVReaderWriter ()
{
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    NSString *delimiterString;
}

@property (nonatomic, assign) FileDelimiter delimiter;

@end

@implementation CSVReaderWriter

#pragma mark - Deprecated methods

- (void)open:(NSString *)path mode:(FileMode)mode
{
    NSError *error;
    [self open:path mode:mode error:&error];
}

- (BOOL)read:(NSMutableString **)column1 column2:(NSMutableString **)column2
{
    NSString *line = [self readLine];
    
    if ([line length] == 0) {
        return false;
    }
    
    NSArray *splitLine = [line componentsSeparatedByString:delimiterString];
    
    if ([splitLine count] < 2) {
        return false;
    }
    else {
        *column1 = [NSMutableString stringWithString:splitLine[0]];
        *column2 = [NSMutableString stringWithString:splitLine[1]];
        return true;
    }
}

- (BOOL)read:(NSMutableArray *)columns
{
    NSString *line = [self readLine];
    
    if ([line length] == 0) {
        return false;
    }
    
    NSArray *splitLine = [line componentsSeparatedByString:delimiterString];
    
    if ([splitLine count] < 2) {
        return false;
    }
    else {
        for (int i = 0; i < splitLine.count; i++) {
            [columns addObject:splitLine[i]];
        }
        return true;
    }
}

- (void)close
{
    [self closeInputStream];
    [self closeOutputStream];
}

#pragma mark - Retained methods

- (void)write:(NSArray *)columns
{
    NSMutableString *output = [NSMutableString string];
    
    for (int i = 0; i < [columns count]; i++) {
        [output appendString: columns[i]];
        if (i < [columns count] - 1) {
            [output appendString:delimiterString];
        }
    }
    
    [self writeLine:output];
}

#pragma mark - New methods

- (BOOL)open:(NSString *)path mode:(FileMode)mode error:(NSError **)error
{
    if (!path || path.length == 0) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:CSVReaderWriterErrorDomain
                                         code:CSVReaderInvalidPath
                                     userInfo:@{NSLocalizedDescriptionKey: @"Invalid path."}];
        }
        return false;
    }
    
    [self setDelimiter:FileDelimiterTab];
    
    if (mode == FileModeRead) {
        inputStream = [NSInputStream inputStreamWithFileAtPath:path];
        
        // check if the created instance of inputStream is not null since the return type of the instance method is nullable
        if (!inputStream && error != NULL) {
            *error = [NSError errorWithDomain:CSVReaderWriterErrorDomain
                                         code:CSVReaderWriterErrorOpenFile
                                     userInfo:@{NSLocalizedDescriptionKey: @"Can not open file."}];
            return false;
        }
        
        [inputStream open];
    }
    else if (mode == FileModeWrite) {
        // this will overwrite an existing file or create a new file if it doesn't exist
        outputStream = [NSOutputStream outputStreamToFileAtPath:path
                                                         append:NO];
        [outputStream open];
    }
    else {
        if (error != NULL) {
            *error = [NSError errorWithDomain:CSVReaderWriterErrorDomain
                                         code:CSVReaderWriterUnknownFileMode
                                     userInfo:@{NSLocalizedDescriptionKey: @"Unknown file mode specified."}];
        }
        return false;
    }
    return true;
}

- (BOOL)readColumns:(NSMutableArray *)columns error:(NSError **)error
{
    NSString *line = [self readLine];
    
    if ([line length] == 0) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:CSVReaderWriterErrorDomain
                                         code:CSVReaderNullFile
                                     userInfo:@{NSLocalizedDescriptionKey: @"File is empty."}];
        }
        return false;
    }
    
    NSArray *splitLine = [line componentsSeparatedByString:delimiterString];
    
    if ([splitLine count] < 2) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:CSVReaderWriterErrorDomain
                                         code:CSVReaderInvalidFormat
                                     userInfo:@{NSLocalizedDescriptionKey: @"Invalid file format."}];
        }
        return false;
    }
    else {
        for (int i = 0; i < splitLine.count; i++) {
            [columns addObject:splitLine[i]];
        }
        return true;
    }
}

- (BOOL)write:(NSArray *)columns error:(NSError **)error
{
    if (!outputStream) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:CSVReaderWriterErrorDomain
                                         code:CSVWriterInvalidPermission
                                     userInfo:@{NSLocalizedDescriptionKey: @"Writing not permitted."}];
        }
        return false;
    }
    
    NSMutableString *output = [NSMutableString string];
    
    for (int i = 0; i < [columns count]; i++) {
        [output appendString: columns[i]];
        if (i < [columns count] - 1) {
            [output appendString:delimiterString];
        }
    }
    
    [self writeLine:output];
    return true;
}

- (void)closeInputStream
{
    if (inputStream) {
        [inputStream close];
        inputStream = nil;
    }
}

- (void)closeOutputStream
{
    if (outputStream) {
        [outputStream close];
        outputStream = nil;
    }
}

#pragma mark - Private methods

- (void)setDelimiter:(FileDelimiter)delimiter
{
    _delimiter = delimiter;
    delimiterString = delimiterArray[_delimiter];
}

- (NSString *)readLine
{
    // if a carriage return '\r' is detected, continue...
    uint8_t ch = 0;
    NSMutableString *str = [NSMutableString string];
    while ([inputStream read:&ch maxLength:1] == 1) {
        if (ch == '\n') {
            break;
        }
        [str appendFormat:@"%c", ch];
    }
    return str;
}

- (void)writeLine:(NSString *)line
{
    NSData *data = [line dataUsingEncoding:NSUTF8StringEncoding];
    
    // NOTE: -outputStrem:write:maxLength: - Returns the number of bytes actually written.
    [outputStream write:data.bytes maxLength:data.length];
    
    unsigned char* lf = (unsigned char*)"\n";
    [outputStream write:lf maxLength: 1];
}

@end
