//
//  CSVReader.h
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 13/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Blocks.h"
#import "CSVFileDelimiter.h"

@interface CSVReader : NSObject

NS_ASSUME_NONNULL_BEGIN

/*
 We have to tag init as unavailable so that whenever a CSVReader instance is made, we are sure that a path is provided.
 */
- (instancetype)init NS_UNAVAILABLE;

- (nullable instancetype)initWithFilePath:(NSString *)path NS_DESIGNATED_INITIALIZER;

- (void)openFile;
- (void)openFileWithSuccessBlock:(_Nullable VoidBlock)successBlock failureBlock:(_Nullable FailureBlock)failureBlock;

// All Read methods returns an empty array for invalid file format.
// We return an immutable array to preserve original data read from file.
// If the data is to be modified, dev should make a mutableCopy of the return value in the class the method is invoked.

/*
 Read all columns in the file.
 No parameters.
 
 returns:
 1) NSArray - reads and return all column.
            - if format is invalid, returns empty array [].
 */
- (NSArray *)readAllColumns;

/*
 Read range of columns specified by the user.
 param:
 1) range   - range of columns.
            - automatically removes invalid index values.
 
 returns:
 1) NSArray - if parameter `indexes` is empty or null, reads and returns all column.
            - if format is invalid, returns empty array [].
 */
- (NSArray *)readColumnsAtRange:(NSRange)range;

/*
 Read specific column/s specified by the user.
 param:
 1) indexes - list of column numbers the user wants to read.
            - automatically removes invalid index values.
 
 returns:
 1) NSArray - if parameter `indexes` is empty or null, reads and returns all column.
            - if format is invalid, returns empty array [].
 */
- (NSArray *)readColumnsAtIndexes:(NSArray<NSNumber *> *)indexes;

/*
 Reads only one column value
 params:
 1) index   - must be in the range of 0...column.count-1
            - automatically removes invalid index values.
 
 returns:
 1) NSString    - returns the column value of the specific index
                - returns nil if the index is invalid
 */
- (NSString *)readColumnAtIndex:(NSUInteger)index;

// MARK: - Close method
- (void)closeFile;

NS_ASSUME_NONNULL_END

@end
