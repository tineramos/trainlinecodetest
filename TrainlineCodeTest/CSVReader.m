//
//  CSVReader.m
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 13/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import "CSVReader.h"

#import "CSVReaderWriterErrors.h"

static NSString * const CSVReaderNewLine = @"\n";

@interface CSVReader ()
{
    NSInputStream *inputStream;
    NSString *delimiterString;
}

@property (nonatomic, assign) FileDelimiter delimiter;

@end

@implementation CSVReader

// MARK: - Designated Initializer

- (instancetype)initWithFilePath:(NSString *)path
{
    if (!path || path.length == 0) {
        return nil;
    }
    
    self = [super init];
    if (self) {
        [self setDelimiter:FileDelimiterTab];
        inputStream = [NSInputStream inputStreamWithFileAtPath:path];
    }
    return self;
}

// MARK: - Public methods

- (void)openFile
{
    [self openFileWithSuccessBlock:nil failureBlock:nil];
}

- (void)openFileWithSuccessBlock:(VoidBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
    if (!inputStream) {
        if (failureBlock) {
            failureBlock(@"Can not open file.");
        }
        return;
    }
    
    // to start the streaming of data from the input source.
    [inputStream open];
    if (successBlock) {
        successBlock();
    }
}

- (NSArray *)readAllColumns
{
    return [self readColumnsAtIndexes:@[]];
}

- (NSString *)readColumnAtIndex:(NSUInteger)index
{
    NSArray *column = [self readColumnsAtIndexes:@[@(index)]];
    if (column.count == 1) {
        return [column firstObject];
    }
    else {
        return nil;
    }
}

- (NSArray *)readColumnsAtRange:(NSRange)range
{
    // convert range to array of indexesa
    NSMutableArray *indexArray = [NSMutableArray array];
    for (NSUInteger i = range.location; i < (range.location + range.length); i++) {
        [indexArray addObject:@(i)];
    }
    return [self readColumnsAtIndexes:indexArray];
}

- (NSArray *)readColumnsAtIndexes:(NSArray<NSNumber *> *)indexes
{
    NSMutableArray *columnArray = nil;
    NSString *line = [self readLine];
    
    if ([line length] > 0) {
        
        NSArray *splitLine = [line componentsSeparatedByString:delimiterString];
        
        if ([splitLine count] > 0) {
            
            columnArray = [NSMutableArray array];
            
            if (indexes.count > 0) {
                
                [self eliminateInvalidIndexesInArray:&indexes
                                withTotalColumnCount:splitLine.count];
                
                for (int i = 0; i < indexes.count; i++) {
                    columnArray[i] = splitLine[[indexes[i] unsignedIntegerValue]];
                }
                
            }
            else {
                // read all columns
                columnArray = [splitLine mutableCopy];
            }
        }
    }
    return columnArray;
}

- (void)closeFile
{
    if (inputStream) {
        [inputStream close];
        inputStream = nil;
    }
}

#pragma mark - Private methods

- (void)setDelimiter:(FileDelimiter)delimiter
{
    _delimiter = delimiter;
    delimiterString = delimiterArray[_delimiter];
}

// this will handle the filtering of passed indexes
// remove all invalid indexes (e.g. negative and out of bounds indexes)
- (void)eliminateInvalidIndexesInArray:(NSArray **)indexArray withTotalColumnCount:(NSUInteger)count
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF > 0 AND SELF < %d", count];
    *indexArray = [*indexArray filteredArrayUsingPredicate:predicate];
}

- (NSString *)readLine
{
    // if a carriage return '\r' is detected, continue...
    uint8_t ch = 0;
    NSMutableString *str = [NSMutableString string];
    while ([inputStream read:&ch maxLength:1] == 1) {
        if (ch == '\n') {
            break;
        }
        [str appendFormat:@"%c", ch];
    }
    return str;
}

@end
