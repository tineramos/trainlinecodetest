//
//  CSVFileDelimiter.m
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 13/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import "CSVFileDelimiter.h"

NSString * const delimiterArray[] = { @"\t", @"," };

@implementation CSVFileDelimiter

@end
