//
//  CSVFileDelimiter.h
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 13/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FileDelimiter) {
    FileDelimiterTab = 0,
    FileDelimiterComma
};

extern NSString * const delimiterArray[];

@interface CSVFileDelimiter : NSObject

@end
