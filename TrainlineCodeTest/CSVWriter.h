//
//  CSVWriter.h
//  TrainlineCodeTest
//
//  Created by Tine Ramos on 13/12/2016.
//  Copyright © 2016 Tine Ramos. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Blocks.h"
#import "CSVFileDelimiter.h"

@interface CSVWriter : NSObject

NS_ASSUME_NONNULL_BEGIN

/*
 We have to tag init as unavailable so that whenever a CSVWriter instance is made, we are sure that a path is provided.
 */
- (instancetype)init NS_UNAVAILABLE;

- (nullable instancetype)initWithFilePath:(NSString *)path NS_DESIGNATED_INITIALIZER;

- (void)openFile;
- (void)openFileWithSuccessBlock:(_Nullable VoidBlock)successBlock failureBlock:(_Nullable FailureBlock)failureBlock;

- (void)write:(NSArray *)columns;

- (void)closeFile;

NS_ASSUME_NONNULL_END

@end
