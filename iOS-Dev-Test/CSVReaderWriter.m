/*
 A junior developer was tasked with writing a reusable implementation for a mass mailing application to read and write text files that hold tab separated data.
 
 His implementation, although it works and meets the needs of the application, is of very low quality.
 
 Your task:
 
 - Identify and annotate the shortcomings in the current implementation as if you were doing a code review, using comments in the source files.
 
 - Refactor the CSVReaderWriter implementation into clean, idiomatic, elegant, rock-solid & well performing code, without over-engineering.
 
 - Where you make trade offs, comment & explain.
 
 - Assume this code is in production and backwards compatibility must be maintained. Therefore if you decide to change the public interface, please deprecate the existing methods. Feel free to evolve the code in other ways though. You have carte blanche while respecting the above constraints. 
 */

#import <Foundation/Foundation.h>

/*
 GENERAL COMMENT:
 I think, it would be better to separate Read and Write functions in two classes.
 Separating the functions, will make the code simpler, direct and understandable and
 whatever the class' tasks are will be more clear since it will be doing/managing only one role at a time.
 By doing this, we can eliminate invalid permission error.
 */

// NS_OPTIONS should only be used for defining bitmask, use NS_ENUM instead
typedef NS_OPTIONS(NSUInteger, FileMode) {
    FileModeRead = 1,
    FileModeWrite = 2   // '= 2' can be removed, succeeding values in an enum can be inferred
};

@interface CSVReaderWriter : NSObject

// instead of declaring the open:mode: as an instance method, make it a init method for the class (see CSVReader and CSVWriter class)
// making this an initializer, will ensure that we have the right properties setup before performing any methods available in the class.
- (void)open:(NSString*)path mode:(FileMode)mode;   // rename openFileWithPath:withMode:

// instead of just returning boolean, pass on an NSError to prompt user of the specific error
- (BOOL)read:(NSMutableString**)column1 column2:(NSMutableString**)column2; //  rename readColumnOne:columnTwo:
- (BOOL)read:(NSMutableArray*)columns;  // rename -readColumns:
- (void)write:(NSArray*)columns;
- (void)close;  // rename to closeStream

@end

@implementation CSVReaderWriter {
    NSInputStream* inputStream;
    NSOutputStream* outputStream;
}

/*
 `path` - can have a null value, add a condition to check if we have a valid value for path (i.e. not null and with length > 0)
 */

- (void)open:(NSString*)path mode:(FileMode)mode {
    
    if (mode == FileModeRead) {
        
        inputStream = [NSInputStream inputStreamWithFileAtPath:path];
        /*
         inputStreamWithFileAtPath: has a return type `nullable`
         so it's better to check if an instance is created using the path provided:
         
         if (inputStream) { [inputStream open] }
         */
        [inputStream open];
    }
    else if (mode == FileModeWrite) {
        outputStream = [NSOutputStream outputStreamToFileAtPath:path
                                                         append:NO];
        [outputStream open];
    }
    else {
        /*
         Exception will invoke a force crash - best way to handle unwanted values/behaviours is to display an error message instead of terminating the app.
         Given that this app is in production, there shouldn't be exceptions, NSError must be used.
        */
        NSException* ex = [NSException exceptionWithName:@"UnknownFileModeException"
                                                  reason:@"Unknown file mode specified"
                                                userInfo:nil];
        @throw ex;
    }
}

- (NSString*)readLine {
    
    /*
     check if we have the necessary permission and objects to read:
     
     if (mode == FileModeRead && inputStream != nil) { ... }
     else { return @""; }
     */
    
    uint8_t ch = 0;
    NSMutableString* str = [NSMutableString string];
    while ([inputStream read:&ch maxLength:1] == 1) {
        // always use braces even when condition body is a one-liner to prevent errors (e.g. when adding more lines in the future, etc.)
        if (ch == '\n')
            break;
        [str appendFormat:@"%c", ch];
    }
    return str;
}

// this method can only read at most two columns at a time.
// this restricts the app/dev in only reading the first two columns - better way is to make a method that reads within a range, or specific indexes.
- (BOOL)read:(NSMutableString**)column1 column2:(NSMutableString**)column2 {
    int FIRST_COLUMN = 0;
    int SECOND_COLUMN = 1;
    
    NSString* line = [self readLine];
    
    // TODO: say something about the nil
    if ([line length] == 0) {
        *column1 = nil;
        *column2 = nil;
        return false;
    }
    
    NSArray* splitLine = [line componentsSeparatedByString: @"\t"]; // declare "\t" this string as constant
    
    // change condition to '< 2' to include the case where there is only one column in the file.
    if ([splitLine count] == 0) {
        *column1 = nil;
        *column2 = nil;
        return false;
    }
    else {
        *column1 = [NSMutableString stringWithString:splitLine[FIRST_COLUMN]];
        *column2 = [NSMutableString stringWithString:splitLine[SECOND_COLUMN]];
        return true;
    }
}

- (BOOL)read:(NSMutableArray*)columns {
    int FIRST_COLUMN = 0;
    int SECOND_COLUMN = 1;
    
    NSString* line = [self readLine];
    
    // remove lines where values are nil. Nil values are not accepted objects in Array.
    // also note that nil is used to terminate an array.
    if ([line length] == 0) {
        columns[FIRST_COLUMN] = nil;
        columns[SECOND_COLUMN] = nil;
        return false;
    }
    
    NSArray* splitLine = [line componentsSeparatedByString: @"\t"];
    
    // change condition to '< 2' to include the case where there is only one column in the file.
    if ([splitLine count] == 0) {
        columns[FIRST_COLUMN] = nil;
        columns[SECOND_COLUMN] = nil;
        return false;
    }
    else {
        columns[FIRST_COLUMN] = splitLine[FIRST_COLUMN];
        columns[SECOND_COLUMN] = splitLine[SECOND_COLUMN];
        return true;
    }
}

- (void)writeLine:(NSString*)line {
    NSData* data = [line dataUsingEncoding:NSUTF8StringEncoding];
    
    const void* bytes = [data bytes];
    [outputStream write:bytes maxLength:[data length]];
    
    // instead of adding new line here, do it in -write: method after for-loop execution??
    unsigned char* lf = (unsigned char*)"\n";
    [outputStream write: lf maxLength: 1];
}

- (void)write:(NSArray*)columns {
    
    /*
     before writing to the file,
     check if we have the necessary permission and objects to read:
     
     if (outputStream != nil) { ... }
     else { return @""; }
     */
    
    // can be initialized as [NSMutableString string];
    NSMutableString* outPut = [@"" mutableCopy];
    
    for (int i = 0; i < [columns count]; i++) {
        [outPut appendString: columns[i]];
        // modify condition to be more descriptive
        if (([columns count] - 1) != i) {
            [outPut appendString: @"\t"];
        }
    }
    
    // Add newline here: [output appendString:@"\n"]; or retain original way of adding new line?
    
    [self writeLine:outPut];
}

// better to separate closing of read/write stream in some cases that they are accessing different file
- (void)close {
    
    // set created instance variable to nil when closing the streams
    
    // nil resolves to NO no need to compare it in condition
    if (inputStream != nil) {
        [inputStream close];
        // inputStream = nil; release inputStream iVar
    }
    
    // duplicate -
    /*
    if (inputStream != nil) {
        [inputStream close];
    }
    */
    // should be:
    if (outputStream != nil) {
        [outputStream close];
        // outputStream = nil; release outputStream iVar
    }
}

@end
